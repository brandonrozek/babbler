$(document).ready(function () {

    //Attempt connection to server
    try {
        var socket = io();
    } catch (e) {}

    //Setup runtime variables
    var user = '';
    var msgCount = 0;
    var muted = false;
    var tuturu = false;
    var icon = "assets/chat.png";
    var loggedin = false;
    var reconnecting = false;
    var recipients = [];
    var onlineUsers = [];
    var ignoring = [];

    //setup static messages
    var messages = {}
    messages.help = "<u>Available Commands</u>\
                    <p> > /Clear: Clears current messages</p>\
                    <p> > /Tuturu: Toggles notification sound</p>\
                    <p> > /OnlineList: Shows users currently online</p>\
                    <p> > /Poke [user] or [all]: Pokes [user] or [all]</p>\
                    <p> > /Ignore [user] or [all]: Prevents messages from [user] or [all]</p>\
                    <p> > /Notice [user] or [all]: Allows messages from [user] or [all]</p>\
                    <p> > /IgnoreList: Shows who you are currently ignoring</p>";
    messages.onlyPerson = "You are the only person online.";
    messages.chooseIgnore = "Please enter an online user to Ignore.";
    messages.ingoreAll = "You are now ignoring everyone.";
    messages.ignoredAll = "You are already ingoring everyone."
    messages.chooseNotice = "Please choose an online user to notice.";
    messages.noticeAll = "You are now noticing everyone.";
    messages.noticedAll = "You are already noticing everyone."
    messages.noneIgnored = "You are not ingoring anyone.";
    messages.notSupported = "Your browser doesn't support this feature.";
    messages.alreadyLogged = "You are logged in someone else.";
    messages.wrongPassword = "The username/password combination you entered is incorrect.";
    messages.pokeEveryone = "You poked everyone.";
    messages.pokeWho = "Please choose someone to poke.";

    //Attepmt to enable notifications
    try {
        if (Notification && Notification.permission !== "denied") {
            Notification.requestPermission(function (status) {
                //Change on user permission
                if (Notification.permission !== status) {
                    Notification.permission = status;
                }
            });
        }
    } catch (e) {}


    //****Start Functions Here****//

    var signin = function () {
        if (userName.val() != '' && $('.signin > input[type=password]').val() != '') {
            user = userName.val();
            //True is to signify connect, False for disconnect
            socket.emit('user', [user, true, $('.signin > input[type=password]').val()]);
            window.setTimeout(function () {
                if (loggedin) {
                    loginField.off();
                    $('.signin > input[type=submit]').off();
                    $('.signin').hide();
                    $('.chat').show();
                    $('.only-me').val(user);
                    //Clear user sensitive info
                    $('.signin > input[type=password]').val("");
                    delete userName;
                    delete loginField;
                }
            }, 250);
        }
    };

    var muteHandler = function () {
        muted = !muted;
        if (muted) {
            $(this).attr('src', 'assets/mute.svg').attr('alt', 'Play Sound?');
        } else {
            $(this).attr('src', 'assets/sound.svg').attr('alt', 'Mute Sound?');
        }
    }

    var showPopup = function (element) {
        if ($(element).css('display') == 'none') {
            $(element).css('display', 'block').addClass('popup-animate');
            if (element == '.list') {
                checkEvents();
            } else if (element == '.emoji-list') {
                $('.emoji-item').on('click', function () {
                    $('.input > input[type=text]').val($('.input > input[type=text]').val() + $(this).text());
                });
            } else if (element == '.menu') {
                $('.start-pic-2').on('click', openImgModal);
                $('.send-pic-link').on('click', function () {
                    $('.input > input[type=text]').val($('.input > input[type=text]').val() + " <img src=\"\"/>");
                });
                $('.send-link').on('click', function () {
                    $('.input > input[type=text]').val($('.input > input[type=text]').val() + " <a href=\"\"></a>");
                });
            }
        } else if ($(element).css('display') == 'block') {
            $(element).css('display', 'none').removeClass('popup-animate');
            if (element == '.list') {
                $('.list input[type=checkbox], .list input[type=radio]').off();
            } else if (element == '.emoji-list') {
                $('emoji-item').off();
            } else if (element == '.menu') {
                $('.send-link, .send-pic-link, .start-pic-2').off()
            }
        }
    };

    //Trashes current list and starts over
    var makeList = function () {
        var wasVisible = ($('.list').css('display') == 'block') ? true : false;
        var everyone = $('.list input[type=radio][value=everyone]').prop('checked');
        $('.list').css('display', 'none').removeClass("popup-animate");
        $('.list > li').has('input[type=checkbox]').remove();
        $('.list').prepend(function () {
            var list = "";
            for (var i = 0; i < onlineUsers.length; i++) {
                list += "<li>" + onlineUsers[i] + "<input value='" + onlineUsers[i] + "' type='checkbox' role='menuitemcheckbox'/></li>";
            }
            return list;
        });

        if (recipients.length > 0) {
            var people = $('.list input[type=checkbox]');
            for (var i = 0; i < people.length; i++) {
                if (recipients.indexOf(people[i].value) != -1) {
                    people[i].checked = true;
                }
            }
        }
        toWho();
        if (wasVisible) {
            showPopup('.list');
        }
    };
    var checkEvents = function () {
        //If an individual person is checked then uncheck everybody
        $('.list input[type=checkbox]').on('click', function () {
            $('.list input[type=radio]').prop('checked', false);
            toWho();
        });
        //If everybody is checked then uncheck individuals
        $('.list input[type=radio]').on('click', function () {
            $('.list input[type=checkbox]').prop('checked', false);
            $('.list input[type=radio]').not(this).prop('checked', false);
            toWho();
        })
    };
    //Mostly grammar function but does add recipients in for loop
    var toWho = function () {
        recipients = [];
        var list = $('.list input[type=checkbox]:checked');
        var everyone = $('.list input[type=radio][value=everyone]').prop('checked')
        if (list.length == 0) {
            if (everyone) {
                $('.to .who').text("To Everyone: ");
            } else {
                $('.to .who').text("Only Me: ")
                $('.list input[type=radio][value=' + user + ']').prop('checked', true);
                recipients.push(user);
            }
        } else {
            for (var i = 0; i < list.length; i++) {
                recipients.push(list[i].value);
            }
            if (list.length == 1) {
                $('.to .who').text("To " + recipients[0] + ": ");
            } else {
                $('.to .who').text("To " + list.length + " people: ");
            }
        }
        if (window.innerWidth <= 900) {
            $('.to .who').text("To?");
        }
    };
    var consoleMsg = function (message, error) {
        if (error) {
            return "<div class='console msg' style='color: #ff341a'><span style='color:white'> > </span>" + message + "</div>";
        } else {
            return "<div class='console msg'><span> > </span>" + message + "</div>";
        }
    }
    var sendMsg = function () {
        if (text.val() != "") {
            if (text.val().toUpperCase().match(/\/\w+/) != null) {
                switch (text.val().toUpperCase().match(/\/\w+/)[0]) {
                case '/HELP':
                    $('article').append(consoleMsg(messages.help, false));
                    autoScroll();
                    playSound();
                    break;
                case '/CLEAR':
                    $('.msg').remove();
                    msgCount = 0;
                    break;
                case '/TUTURU':
                    tuturu = !tuturu;
                    if (tuturu) {
                        $('#sound-unfocused').attr('src', 'assets/Tuturu.mp3');
                    } else {
                        $('#sound-unfocused').attr('src', 'assets/message.mp3');
                    }
                    $('article').append(function () {
                        var message = (tuturu) ? "Tuturu mode enabled" : "Tuturu mode disabled";
                        return consoleMsg(message, false);
                    });
                    autoScroll();
                    playSound();
                    break;
                case '/ONLINELIST':
                    $('article').append(function () {
                        var online = (onlineUsers.length > 0) ? "Online Users: " + onlineUsers.join(", ") : messages.onlyPerson;
                        return consoleMsg(online, false);
                    });
                    autoScroll();
                    playSound();
                    break;
                case '/POKE':
                    var person = text.val().slice(text.val().toUpperCase().indexOf("/POKE") + 5).match(/\w+/);
                    if (person != null && person[0].toUpperCase() == 'ALL') {
                        socket.emit('poke', [user, "ALL"]);
                        $('article').append(function () {
                            return consoleMsg(messages.pokeEveryone, false);
                        });
                    } else if (person != null && onlineUsers.indexOf(person[0]) != -1) {
                        socket.emit('poke', [user, person[0]]);
                        $('article').append(function () {
                            return consoleMsg("You poked " + person[0], false);
                        });
                    } else {
                        $('article').append(consoleMsg(messages.pokeWho, false));
                    }
                    autoScroll();
                    playSound();
                    break;
                case '/IGNORE':
                    var person = text.val().slice(text.val().toUpperCase().indexOf("/IGNORE") + 7).match(/\w+/);
                    if (person != null && person[0].toUpperCase() == 'ALL') {
                        if (ignoring == onlineUsers) {
                            $('article').append(function () {
                                return consoleMsg(messages.ignoredAll, false);
                            });
                        } else {
                            ignoring = onlineUsers;
                            $('article').append(function () {
                                return consoleMsg(messages.ingoreAll, false);
                            });
                        }
                    } else if (person != null && onlineUsers.indexOf(person[0]) != -1) {
                        if (ignoring.indexOf(person[0]) != -1) {
                            $('article').append(function () {
                                return consoleMsg("You are already ignoring " + person[0], false);
                            });
                        } else {
                            ignoring.push(person[0]);
                            $('article').append(function () {
                                return consoleMsg("Now ignoring " + person[0], false);
                            })
                        }
                    } else {
                        $('article').append(consoleMsg(messages.chooseIgnore, false));
                    }
                    autoScroll();
                    playSound();
                    break;
                case '/NOTICE':
                    var person = text.val().slice(text.val().toUpperCase().indexOf("/NOTICE") + 7).match(/\w+/);
                    if (person != null && person[0].toUpperCase() == 'ALL') {
                        if (ignoring.length == 0) {
                            $('article').append(function () {
                                return consoleMsg(messages.noticedAll, false);
                            });
                        } else {
                            ignoring = [];
                            $('article').append(function () {
                                return consoleMsg(messages.noticeAll, false);
                            });
                        }
                    } else if (person != null && onlineUsers.indexOf(person[0] != -1)) {
                        if (ignoring.indexOf(person[0]) == -1) {
                            $('article').append(function () {
                                return consoleMsg("You are already noticing " + person[0], false);
                            });
                        } else {
                            ignoring.splice(ignoring.indexOf(person[0]), 1);
                            $('article').append(function () {
                                return consoleMsg("Now noticing " + person[0], false);
                            })
                        }
                    } else {
                        $('article').append(consoleMsg(messages.chooseNotice, false));
                    }
                    autoScroll();
                    playSound();
                    break;
                case '/IGNORELIST':
                    $('article').append(function () {
                        var ignored = (ignoring.length > 0) ? "Ignoring: " + ignoring.join(", ") : messages.noneIgnored;
                        return consoleMsg(ignored, false);
                    });
                    autoScroll();
                    playSound();
                    break;
                default:
                    socket.emit('message', [user, text.val(), recipients]);
                }
            } else {
                socket.emit('message', [user, text.val(), recipients]);
            }
            text.val("");
        }
    };
    var messageAppear = function (author, message, recipients) {
        if (ignoring.indexOf(author) != -1) {
            return false;
        }
        var Time = new Date;
        var filteredTime = (author == "Users Online") ? "" : filterTime(Time);
        var a = (author == user) ? 'my' : 'reply';
        var b = (recipients && recipients.length > 0) ? "<span style='font-style: italic;'>" + author + " to " + recipients.join(", ") + ": </span>" : "<span>" + author + ": </span>";
        $('article').append(function () {
            return "<div class='" + a + " msg'>" + b + message + "<datetime class='msg-time'>" + filteredTime + "</datetime></div>";
        });
        return true;
    }
    var filterTime = function (time) {
        var hours = time.getHours();
        var minutes = time.getMinutes();
        var m = (hours > 12) ? " PM" : " AM";
        hours = (hours > 12) ? hours % 12 : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        return hours + ":" + minutes + m;
    }
    var messageHandler = function () {
        $('.msg').off();
        $('.msg').not('.console').on('mouseenter', function () {
            $(this).children('datetime').css('display', 'inline-block');
        })
        $('.msg').not('.console').on('mouseleave', function () {
            $(this).children('datetime').css('display', 'none');
        })
    }
    var autoScroll = function () {
        msgCount++;
        $('.msg')[msgCount - 1].scrollIntoView();
    };
    var playSound = function () {
        if (!muted) {
            if (document.hasFocus()) {
                document.getElementById('sound-focused').play();
            } else {
                document.getElementById('sound-unfocused').play();
            }
        }
    };
    var closeNotif = function () {
        window.setTimeout(function () {
            notification.close();
        }, 5000);
    };
    var filterMessage = function (message) {
        var emoticonStart = "<object type='image/svg+xml' class='emoticon' data='";
        var emoticonEnd = "'>";
        var objectEnd = "</object>";
        return message
            .replace(/;\(/g, emoticonStart + "assets/crying.svg" + emoticonEnd + ";(" + objectEnd)
            .replace(/:\)/g, emoticonStart + "assets/smile.svg" + emoticonEnd + ":)" + objectEnd)
            .replace(/:\(/g, emoticonStart + "assets/sad.svg" + emoticonEnd + " :(" + objectEnd)
            .replace(/:D/ig, emoticonStart + "assets/laugh.svg" + emoticonEnd + ":D" + objectEnd)
            .replace(/:O/ig, emoticonStart + "assets/surprised.svg" + emoticonEnd + ":O" + objectEnd)
            .replace(/:P/ig, emoticonStart + "assets/cheeky.svg" + emoticonEnd + ":P" + objectEnd)
            .replace(/;\)/g, emoticonStart + "assets/wink.svg" + emoticonEnd + ";)" + objectEnd)
            //Security replaces
            .replace(/<script(.*?)>/ig, '')
            .replace(/<style(.*?)>/ig, '')
            .replace(/<link(.*?)>/ig, '')
            .replace(/onload/ig, 'on-load')
            .replace(/onunload/ig, 'on-unload')
            .replace(/ondblclick/ig, 'on-dblclick')
            .replace(/onclick/ig, 'on-click')
            .replace(/onmousedown/ig, 'on-mousedown')
            .replace(/onmouseup/ig, 'on-mouseup')
            .replace(/onmouseover/ig, 'on-mouseover')
            .replace(/onmouseout/ig, 'on-mouseout')
            .replace(/onfocus/ig, 'on-focus')
            .replace(/onblur/ig, 'on-blur')
            .replace(/onkeypress/ig, 'on-keypress')
            .replace(/onkeyup/ig, 'on-keyup')
            .replace(/onkeydown/ig, 'on-keydown')
            .replace(/onsubmit/ig, 'on-submit')
            .replace(/onreset/ig, 'on-reset')
            .replace(/onselect/ig, 'on-select')
            .replace(/onchange/ig, 'on-change')
            .replace(/onhelp/ig, 'on-help')
            .replace(/onabort/ig, 'on-abort')
            .replace(/onerror/ig, 'on-error')
            // .replace(/http:\/\/\S+/, '<a href="' + message.match(/http:\/\/\S+/) + '">' + message.match(/http:\/\/\S+/) +'</a>')
            //Opens link in new tab
            .replace(/<a/g, '<a target="_blank"')
    }

    var openImgModal = function () {
        if ($('.img-modal, .fade-background').css('display') == 'none') {
            $('.send-pic').text("Send " + $('.to .who').text());
            $('.img-modal, .fade-background').css('display', 'block').addClass('modal-animate');
            $('.send-pic').on('click', function () {
                var canvas = document.getElementsByClassName('img-modal-canvas')[0];
                socket.emit('message', [user, "<img src='" + canvas.toDataURL() + "'/>", recipients]);
                $('.fade-background').click();
            });
            $('.fade-background, .img-modal-close').on('click', function () {
                $('.camera-error').remove();
                $('.img-modal, .fade-background').css('display', 'none').removeClass('modal-animate');
                $('.fade-background, .send-pic, .img-modal-close').off();
                clearInterval(picInterval);
                var video = document.getElementsByClassName('img-modal-video')[0];
                video.pause();
                video.src = "";
            })
            initiateVideoCapture();
        }
    }

    var initiateCanvas = function () {
        var video = document.getElementsByClassName('img-modal-video')[0];
        var canvas = document.getElementsByClassName('img-modal-canvas')[0];
        if (video.played.length > 0) {
            canvas.width = video.videoWidth;
            canvas.height = video.videoHeight;
            context = canvas.getContext('2d');
            picInterval = setInterval(function () {
                if (canvas.width > 0 && canvas.height > 0) {
                    context.drawImage(video, 0, 0, canvas.width, canvas.height);
                }
            }, 60)
        } else {
            setTimeout(initiateCanvas, 500)
        }
    }
    var videoCaptured = function (localMediaStream) {
        var video = document.getElementsByClassName('img-modal-video')[0];
        video.src = window.URL.createObjectURL(localMediaStream);
        video.play();
        setTimeout(function () {
            initiateCanvas();
        }, 500)
        $('.fade-background, .img-modal-close').on('click', function () {
            localMediaStream.stop();
        })
    }
    var initiateVideoCapture = function () {
        navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
        if (navigator.getUserMedia) {
            navigator.getUserMedia(
                // constraints
                {
                    video: true,
                    audio: false
                },
                // successCallback
                videoCaptured,
                function (err) {
                    //Show error message that occured
                    $('.img-modal-video').before("<p class='error camera-error'>" + err + "</p>");
                });
        } else {
            //getUserMedia isn't supported
            $('.img-modal-video').before("<p class='error camera-error'>" + messages.notSupported + "</p>");
        }
    }
    var reLogin = function () {
        if (loggedin) {
            //socket.emit waits until reconnection to send user info
            setTimeout(function () {
                $('.signin-modal, .fade-background').css('display', 'block').addClass('modal-animate');
                $('.signin-modal > input[type=password]').focus();
                $('.signin-modal > input[type=password]').on('keyup', function (e) {
                    if (e.keyCode == 13) {
                        socket.emit('user', [user, true, $('.signin-modal > input[type=password]').val()]);
                        $('.signin-modal > input[type=password]').val("").off();
                        $('.signin-modal, .fade-background').css('display', 'none').removeClass('modal-animte');
                    }
                })
            }, 750);
        }
    };

    //****Start Event Listeners Here****//

    var userName = $('.signin > input[type=text]');
    var loginField = $('.signin > input[type=text], .signin > input[type=password]');

    loginField.on('keyup', function (e) {
        if (e.keyCode == 13) {
            signin();
        }
    });
    $('.signin > input[type=submit]').on('click', function () {
        signin();
    });

    $('.sound').on('click', muteHandler);

    $('.to .who').on('click', function () {
        showPopup('.list');
    });
    $('.open-emoji').on('click', function () {
        showPopup('.emoji-list');
    });
    $('.open-menu').on('click', function () {
        showPopup('.menu');
    });
    $('.start-pic').on('click', openImgModal);
    $('.chat > input[type=submit]').on('click', function () {
        sendMsg();
    });

    var text = $('.input > input[type=text]');

    text.on('keyup', function (e) {
        if (e.keyCode == 13) {
            sendMsg();
        }
    });

    socket.on('user', function (info) {
        if (info[0] == user && !info[1] && !loggedin) {
            //Error at login screen
            $('.signin-error').hide();
            $('.signin-error').fadeIn(500);
            return;
        }
        if (info[0] == user && !info[1] && reconnecting) {
            //Error at reconnect
            $('article').append(consoleMsg(messages.wrongPassword, true));
            reLogin();
            return;
        }
        if (info[0] == user && info[1] && loggedin && !reconnecting) {
            //Already logged in
            playSound();
            $('article').append(consoleMsg(messages.alreadyLogged, true));
            return;
        }
        if (!loggedin) {
            loggedin = true;
        }
        if (reconnecting) {
            reconnecting = false;
        }
        if (info[1]) {
            if (info[0] != user) {
                onlineUsers.push(info[0]);
            }
            var b = " has connected.";
        } else {
            onlineUsers.splice(onlineUsers.indexOf(info[0]), 1)
            var b = " has disconnected.";
        }
        $('article').append(consoleMsg(info[0] + b, false));
        makeList();
        autoScroll();
        playSound();
        if (!document.hasFocus()) {
            try {
                notification.close();
            } catch (e) {}
            notification = new Notification(info[0] + b, {
                icon: icon
            });
            closeNotif();
        }

    });

    //****Start Socket Listeners Here****//

    socket.on('message', function (info) {

        if (info[0] == "Users Online") {
            onlineUsers = info[1].split(', ');
            makeList();
        }

        //Defines the message and implements emoticon system, also makes it so links opens a new tab
        var message = info[1];
        message = filterMessage(message);

        if (info[0] == 'Console') {
            var appeared = true;
            $('article').append(consoleMsg(message, false))
        } else {
            var appeared = messageAppear(info[0], message, info[2]);
        }

        if (appeared) {
            messageHandler();
            autoScroll();
            playSound();
            if (!document.hasFocus()) {
                try {
                    notification.close();
                } catch (e) {}
                notification = new Notification("New Message from " + info[0], {
                    body: info[1],
                    icon: icon
                });
                closeNotif();
            }
        }
    });

    socket.on('poke', function (info) {
        $('article').append(consoleMsg(info + " poked you.", false));
    });

    //Handles disconnects
    socket.on('error', function (error) {
        if (!loggedin) {
            window.location.reload();
            return;
        }
        console.log(error);
        playSound();
        $('article').append(consoleMsg(error, true));
        autoScroll();
        reconnecting = true;
        reLogin();
    });
    $(window).on("resize", function () {
            if (window.matchMedia("(max-width: 900px)").matches) {
                $('.to .who ').text("To?");
            } else {
                toWho();
            }
        })
        //Logs off user and clears notification as window closes
    $(window).unload(function () {
        if (user !== '') {
            socket.disconnect();
        }
        try {
            notification.close();
        } catch (e) {}
    });
});
