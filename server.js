//Current setup is really hard to scale

//username is the unique identifier
//loggedin to ensure only 1 instance is available
//id is to validate without sending password
function makeUser(username, password) {
    this.username = username;
    this.password = password;
    this.loggedin = false;
    this.id = "";
}
var users = [];
users[0] = new makeUser("Brandon", "b27e1dacfa779a6fd07a57583b5bdbe6df57ccb06da30861a077e69ce7032e57d56041f29987c241ca252788fe4980b93d4223ec9be8b59569988b34e84b8da0");
users[1] = new makeUser("delfishious", "b8b4f34615ce0c4fadd2f5f5736418f3daa6e72edd06526c1ae038cd728cba085d6bf333a9b70d338d44fbdfa5fba5598c536e656ed4345f3f2744f28bf8aa57");
users[2] = new makeUser("Gunman28", "65f2a6dbee64cc48761de12dc6a39648b7c76b76db6edc3148784c7dbff3e9417c8321a88b30f4391defb80449b23fd545969049522234dc815ad04bf94deb61");
users[3] = new makeUser("Shadow", "6f6273025a2105f1414f9ddccb4627e2be1e3439500803a440052797a42ce5c9d7b2c5e5781e862636e068d7f7cc194671da252ab0338044b5b3ec9b8c6dd5b4");
users[4] = new makeUser("cnculko", "767ac822cd06d87208feedfa0c6be198943da7605e830c825c06d8a16b11f65bab112ac5c859cb7e52bafaf85feee29544a2d6ff2531cfb6f3a048986304f552");
users[5] = new makeUser("Snoweyangel", "9b6ce1161bdf56864e4a61e593613da8a9faaf68f45127d8659325878e2623280f5f808ae775e3d2602894812eb0f5bd1df95649a9f5b828286ebcce081771d7");
users[6] = new makeUser("Watson", "10c3c50188780403409e8fb3acca2a536ac29eb45f26be28e76bf3929c6075b87e0368076d7b105fab058cfea69fd3aaa011087f1ec5d1286bbbcd353f576028");
users[7] = new makeUser("Jojo", "fadab56021661bb72f36bf57553423a59a482dc8647ab0fbf7fe608c9dc689e2373f1ae3bae426e7751cc0d7034727a578e2a4b090261bf9a4f24aad9d5216e0");
users[8] = new makeUser("Tikiman", "2417f79af92135027b74c719b3db06d0b083342f79ea98a4716f3401f0089417e283c1cd74b54b356b95b22ebed2750a9e56b8128b6c8116cf516ec0fa65ebe9");
users[9] = new makeUser("Celia", 'a3ea24fe8cc41f8a4c21910f9b2028b1c3110fd0f2f72ab545c30ec504323cd9215e0612dfaabb5ae5d277e7bfc9263dfe5611f5930bf3ad42b3b5044d4092e8');

//Faster way to list users for login 
var onlineUsers = [];
//Maximum amount of messages to be sent in a second [Tested that human max is ~8msg/sec]
var maxThrottle = 5;
var uploadLimit = '5mb';

var grabUser = function (username) {
    for (var i = 0; i < users.length; i++) {
        if (username == users[i].username) {
            return users[i];
        }
    }
}

var crypto = require('crypto');
var hash = function (password) {
    return (typeof (password) == 'string') ? crypto.createHash("sha512").update(password, "utf8").digest("hex") : false;
}

var port = Number(process.env.PORT || 5000);
process.env.PWD = process.cwd();

var compression = require('compression');
var getRawBody = require('raw-body');
var express = require('express');
var app = express();

app.use(compression());

//Limits upload
app.use(function (request, response, next) {
    getRawBody(request, {
        limit: uploadLimit
    }, function (err, string) {
        if (err) {
            return next(err);
        }
        request.text = string;
        next();
    })
})

//Add this line for the ability to show static files + relative links
app.use(express.static(process.env.PWD + '/public', {
    dotfiles: 'deny',
    maxAge: '1h'
}));

app.get('/', function (request, response) {
    response.sendFile('index.html');
});

//404s
app.use(function (request, response, next) {
    response.status(404).send('Sorry cant find that!');
});

// app.listen(5000, function () {
//     console.log("Listening on port 5000");
// })

// Start server and initiate Socket.io
var io = require('socket.io').listen(app.listen(port, function () {
    console.log("Listening on port " + port);
}))

io.sockets.on('connection', function (socket) {
    socket.throttle = {
        lastTime: 0,
        warning: 0
    };
    //Throttling system (limits amount of messages that can be sent every 1000 miliseconds)
    var throttle = function (id) {
        if (socket.throttle.lastTime) {
            if (Date.now() - socket.throttle.lastTime < 1000) {
                socket.throttle.warning += 1;
                //Slowly clear throttle
                setTimeout(function () {
                    if (socket.throttle.warning >= 1) {
                        socket.throttle.warning -= 1;
                    }
                }, 1000);
                //Determined that sender is a robot; disconnect so server doesn't overload [determined at more than 8msg/sec]
                if (socket.throttle.warning >= 10) {
                    io.to(id).emit('error', 'Bot detected');
                    socket.disconnect();
                }
                //Determined that sender might be a human and asks to slow down
                if (socket.throttle.warning >= maxThrottle) {
                    io.to(id).emit('message', ['Console', 'You sent your message too fast for us, please wait a couple seconds and try again.']);
                    socket.throttle.lastTime = Date.now();
                    return true;
                }
            }
        }
        socket.throttle.lastTime = Date.now();
        return false;
    }
    socket.on('user', function (info) {
        if (!throttle(this.id)) {
            var verifiedUser = false;
            verifiedUser = grabUser(info[0]);
            //Checks Object and password
            if (verifiedUser && verifiedUser.password == hash(info[2])) {
                //Checks to see if user was logged in before
                if (verifiedUser.loggedin) {
                    io.to(this.id).emit('user', [verifiedUser.username, true]);
                    io.to(verifiedUser.id).emit('user', [verifiedUser.username, true]);
                } else {
                    for (var i = 0; i < onlineUsers.length; i++) {
                        var recipient = grabUser(onlineUsers[i]);
                        if (recipient.username != verifiedUser.username) {
                            io.to(recipient.id).emit('user', [verifiedUser.username, true]);
                        }
                    }
                    io.to(this.id).emit('user', [verifiedUser.username, true]);
                    console.log(info[0] + " has connected");
                }
                //Setup user runtime variables
                verifiedUser.loggedin = true;
                verifiedUser.id = this.id;
                socket.user = verifiedUser;
                //Lists current users
                if (onlineUsers.indexOf(verifiedUser.username) != -1) {
                    onlineUsers.splice(onlineUsers.indexOf(verifiedUser.username), 1);
                }
                if (onlineUsers.length > 0) {
                    io.to(this.id).emit('message', ['Users Online', onlineUsers.join(', ')]);
                } else {
                    io.to(this.id).emit('message', ['Console', 'You are the only person online.']);
                }
                //Adds to online user pool if not already
                if (onlineUsers.indexOf(verifiedUser.username) == -1) {
                    onlineUsers.push(verifiedUser.username);
                }
            } else {
                io.to(this.id).emit("user", [info[0], false]);
            }
        }
    });
    socket.on('message', function (info) {
        if (!throttle(this.id)) {
            User = grabUser(info[0]);
            //Verifies the id from login to confirm (like token)
            if (typeof (User) != 'undefined' && typeof (User.id) != 'undefined' && User.id == this.id) {
                //Are recipients specified?
                if (info[2] && info[2].length > 0) {
                    try {
                        for (var i = 0; i < info[2].length; i++) {
                            var recipient = grabUser(info[2][i]);
                            if (recipient.username != User.username) {
                                io.to(recipient.id).emit('message', info);
                            }
                        }
                        io.to(this.id).emit('message', info);
                    } catch (e) {}
                } else {
                    for (var i = 0; i < onlineUsers.length; i++) {
                        var recipient = grabUser(onlineUsers[i]);
                        if (recipient.username != User.username) {
                            io.to(recipient.id).emit('message', info);
                        }
                    }
                    io.to(this.id).emit('message', info);
                }
            } else {
                io.to(this.id).emit('error', 'Invalid token');
            }
        }
    })

    socket.on('poke', function (info) {
        if (!throttle(this.id)) {
            User = grabUser(info[0]);
            //Verifies the id from login to confirm (like token)
            if (typeof (User) != 'undefined' && typeof (User.id) != 'undefined' && User.id == this.id) {
                if (info[1] == 'ALL') {
                    for (var i = 0; i < onlineUsers.length; i++) {
                        var recipient = grabUser(onlineUsers[i]);
                        if (recipient.username != User.username) {
                            io.to(recipient.id).emit('poke', info[0]);
                        }
                    }
                } else {
                    io.to(grabUser(info[1]).id).emit('poke', info[0]);
                }
            } else {
                io.to(this.id).emit('error', 'Invalid token');
            }
        }
    });


    //Clean up user variables
    socket.on('disconnect', function () {
        try {
            var User = socket.user;
            if (User.id == this.id) {
                User.id = "";
                User.loggedin = false;
                for (var i = 0; i < onlineUsers.length; i++) {
                    var recipient = grabUser(onlineUsers[i]);
                    if (recipient.username != User.username) {
                        io.to(recipient.id).emit('user', [User.username, false]);
                    }
                }
                //Removes user from online users pool
                var onlineIndex = onlineUsers.indexOf(User.username);
                if (onlineIndex != -1) {
                    onlineUsers.splice(onlineIndex, 1);
                }
            }
            console.log(User.username + " has disconnected");
        } catch (e) {}
    });
});
